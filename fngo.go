// Package fngo contains many functions and types to help with a more
// functional style of programming.
package fngo

import "golang.org/x/exp/constraints"

// Number is a type interface meant to be either integer or float.
type Number interface {
	constraints.Integer | constraints.Float
}

// Tuple is a struct containing 2 values of 2 types (or the same type).
type Tuple[O, T any] struct {
	O O
	T T
}

// NewTuple returns a Tuple[O, T] with the values provided.
func NewTuple[O, T any](o O, t T) Tuple[O, T] {
	return Tuple[O, T]{o, t}
}

// Identity returns the argument passed to it.
func Identity[T any](id T) T {
	return id
}

// Empty returns if a list is empty.
func Empty[T any](coll []T) bool {
	return len(coll) == 0
}

// Equal checks if the elements of two lists are equal.
func Equal[T comparable](x, y []T) bool {
	if len(x) != len(y) {
		return false
	}
	for i, elem := range x {
		if elem != y[i] {
			return false
		}
	}
	return true
}

// MapEqual checks if the elements of two maps are equal.
func MapEqual[K, V comparable](x, y map[K]V) bool {
	if len(x) != len(y) {
		return false
	}
	for k, v := range x {
		if yv, ok := y[k]; !ok || yv != v {
			return false
		}
	}
	return true
}

// First returns the first element of a list.
func First[T any](coll []T) T {
	// FIXME: re-create Maybe monad?
	return coll[0]
}

// Last returns the last element of a list.
func Last[T any](coll []T) T {
	return coll[len(coll)-1]
}

// Rest returns every element of a list besides the Head.
func Rest[T any](coll []T) []T { return coll[1:] }

// Take returns the list up to n elements.
func Take[T any](n int, coll []T) []T {
	if n > len(coll) {
		return coll
	}
	return coll[:n]
}

// ButLast returns all elements of a list except for the last n amount.
func ButLast[T any](n int, coll []T) []T {
	return coll[:len(coll)-n]
}

// Drop returns the list after the first n elements.
func Drop[T any](n int, coll []T) []T {
	if n > len(coll) {
		return []T{}
	}
	return coll[n:]
}

// Contains checks if a list contains an element.
func Contains[T comparable](coll []T, want T) bool {
	for _, elem := range coll {
		if elem == want {
			return true
		}
	}
	return false
}

// Filter removes all elements from a list according to f.
func Filter[T any](f func(T) bool, coll []T) []T {
	out := []T{}
	for _, elem := range coll {
		if f(elem) {
			out = append(out, elem)
		}
	}
	return out
}

// Range returns a list starting at start and doesn't go past end, stepping
// in intervals of step.
func Range(start, end, step int) []int {
	out := make([]int, (end-start)/step)
	for i, j := start, 0; i <= end && i+step <= end; i += step {
		out[j] = i
		j++
	}
	return out
}

// RangeTo returns Range(0, n, 1)
func RangeTo(end int) []int {
	return Range(0, end, 1)
}

// Partial returns a function with 1 argument, where f has two arguments,
// and the first of the arguments is passed to Partial.
func Partial[I, A, O any](f func(I, A) O, p I) func(A) O {
	return func(in A) O { return f(p, in) }
}

// Partial returns a function with 1 argument, where f has two arguments,
// and the second of the arguments is passed to Partial.
func PartialSecond[I, A, O any](f func(I, A) O, p A) func(I) O {
	return func(in I) O { return f(in, p) }
}

// ZipWith takes a function f that returns a Tuple[O, T], a list of Os, and a
// list of Ts, and merges the two lists with f.
func ZipWith[O, T any](
	f func(O, T) Tuple[O, T],
	ones []O,
	twos []T,
) []Tuple[O, T] {
	lesslen := 0
	if os, ts := len(ones), len(twos); os > ts {
		lesslen = ts
	} else {
		lesslen = os
	}
	out := make([]Tuple[O, T], lesslen)

	for i := 0; i < lesslen; i++ {
		out[i] = f(ones[i], twos[i])
	}

	return out
}

// Zip calls ZipWith, using NewTuple[O, T] as the function.
func Zip[O, T any](ones []O, twos []T) []Tuple[O, T] {
	return ZipWith(NewTuple[O, T], ones, twos)
}

// Append is a helper functions to be able to use append as a function.
func Append[T any](coll []T, add T) []T { return append(coll, add) }

// String takes a rune and converts it to a string.
func String(a rune) string { return string(a) }

// Rune takes an int and converts it to a rune.
func Rune(a int) rune { return rune(a) }

// Inc increments a number by 1.
func Inc[T Number](a T) T { return a + 1 }

// Dec decrements a number by 1.
func Dec[T Number](a T) T { return a - 1 }

// Adds a and b.
func Add[T Number](a, b T) T { return a + b }

// Subtracts a and b.
func Sub[T Number](a, b T) T { return a - b }

// Multiplies a and b.
func Mul[T Number](a, b T) T { return a * b }

// Divides a and b.
func Div[T Number](a, b T) T { return a / b }

// Applies modulus to a and b.
func Mod(a, b int) int { return a % b }

// Returns if a is zero.
func Zero[T Number](a T) bool { return a == 0 }

// Returns if a is even.
func Even(a int) bool { return a%2 == 0 }

// Returns if a is odd.
func Odd(a int) bool { return a%2 != 0 }

// Returns if a is equal to b.
func EQ[T comparable](a, b T) bool { return a == b }

// Returns if a is not equal to b.
func NEQ[T comparable](a, b T) bool { return a != b }

// Returns if a is greater than b.
func GT[T Number](a, b T) bool { return a > b }

// Returns if a is less than b.
func LT[T Number](a, b T) bool { return a < b }

// Returns the length of a.
func Len[T any](a []T) int { return len(a) }

// Returns the length of string a.
func StrLen(a string) int { return len(a) }

// ReduceLeft takes a function f, with an accumulator and the current element of
// the list. The accumulator is initialized to the init argument, and is
// returned after the function. Goes left to right.
func ReduceLeft[I, O any](f func(O, I) O, init O, in []I) O {
	out := init
	for _, elem := range in {
		out = f(out, elem)
	}
	return out
}

// ReduceRight takes a function f, with an accumulator and the current element of
// the list. The accumulator is initialized to the init argument, and is
// returned after the function. Goes right to left.
func ReduceRight[I, O any](f func(O, I) O, init O, in []I) O {
	out := init
	for i := len(in) - 1; i >= 0; i-- {
		out = f(out, in[i])
	}
	return out
}

// Reductions takes a funciton f, with an accumulator and the current element
// of the list. The accumulator is initialized to the init argument, and each
// iteration of the accumulator is returned after the function as a list.
// Another common name for Reductions is Scan.
func Reductions[I, O any](f func(O, I) O, init O, in []I) []O {
	out := make([]O, len(in))

	if len(out) == 0 {
		return out
	}
	out[0] = f(init, in[0])
	if len(out) == 1 {
		return out
	}

	for i := 1; i < len(in); i++ {
		out[i] = f(out[i-1], in[i])
	}
	return out
}

// Map takes a function f, with the current element, returning an output
// element, and applies f to all elements in the input list, returning a list
// of the output type.
func Map[I, O any](f func(I) O, mp []I) []O {
	out := make([]O, len(mp))
	for i, elem := range mp {
		out[i] = f(elem)
	}
	return out
}

// MapMap is like Map, but for maps! :D
func MapMap[K, O comparable, V, A any](
	f func(K, V) (O, A),
	mp map[K]V,
) map[O]A {
	out := make(map[O]A)
	for k, v := range mp {
		nk, nv := f(k, v)
		out[nk] = nv
	}
	return out
}

// Keys returns all of the keys within a map.
func Keys[K comparable, V any](mp map[K]V) []K {
	out := make([]K, len(mp))
	i := 0
	for k := range mp {
		out[i] = k
		i++
	}
	return out
}

// Vals returns all of the values within a map.
func Vals[K comparable, V any](mp map[K]V) []V {
	out := make([]V, len(mp))
	i := 0
	for _, v := range mp {
		out[i] = v
		i++
	}
	return out
}

// Frequencies returns a map of all the different elements in a list and how
// many occurrences there are of each element.
func Frequencies[T comparable](coll []T) map[T]int {
	fn := func(acc map[T]int, add T) map[T]int {
		acc[add]++
		return acc
	}
	return ReduceLeft(fn, map[T]int{}, coll)
}

// Distinct returns a list with any duplicates removed.
func Distinct[T comparable](coll []T) []T {
	df := func(acc []T, add T) []T {
		if Contains(acc, add) {
			return acc
		}
		return append(acc, add)
	}
	return ReduceRight(df, []T{}, coll)
}

// PartitionBy takes a function to compare the previous and current value
// and a collection to apply with. If the function returns true, a new sub-list
// is created in the partitioned list.
func PartitionBy[T any](f func(T, T) bool, coll []T) [][]T {
	pbf := func(acc [][]T, add T) [][]T {
		if f(Last(Last(acc)), add) {
			acc = append(acc, []T{add})
		} else {
			if len(acc) == 0 {
				acc = append(acc, []T{add})
			} else {
				tmp := append(acc[len(acc)-1], add)
				acc[len(acc)-1] = tmp
			}
		}
		return acc
	}
	return ReduceLeft(pbf, [][]T{{First(coll)}}, Rest(coll))
}

// PartitionStep returns a list of lists of the input list separated
// (or "partitioned") into the smaller equal-length sub-lists, scanned by the
// amount left after stepping.
func PartitionStep[T any](n, step int, coll []T) [][]T {
	out := [][]T{}
	for !Empty(coll) && len(coll) >= n {
		out = append(out, Take(n, coll))
		coll = Drop(step, coll)
	}
	return out
}

// Partition is like PartitionStep, but the step is the same number as n.
func Partition[T any](n int, coll []T) [][]T {
	return PartitionStep(n, n, coll)
}

// Dedupe removes successive duplicates of a value.
func Dedupe[T comparable](coll []T) []T {
	return Map(First[T], PartitionBy(NEQ[T], coll))
}

// Juxt takes an input value and a list of functions, returning a list of the
// return values from the functions being called on the input value
// individually.
func Juxt[I, O any](in I, fs ...func(I) O) []O {
	jf := func(acc []O, f func(I) O) []O {
		return append(acc, f(in))
	}
	return ReduceLeft(jf, []O{}, fs)
}

// SplitAt takes a collection and splits it into two sub-lists, split at the
// index of n.
func SplitAt[T any](n int, coll []T) [][]T {
	return [][]T{Take(n, coll), Drop(n, coll)}
}

// GroupBy takes a monadic function f that returns a key for the returned map.
// f is applied to each element of coll, and element is put in a list at the
// return value of f in the map.
func GroupBy[K comparable, V any](f func(V) K, coll []V) map[K][]V {
	out := map[K][]V{}
	for _, elem := range coll {
		key := f(elem)
		out[key] = append(out[key], elem)
	}
	return out
}

// Iterate takes a monadic function f and calls f(Last(returnList)) recursively
// until limit is met. [ival, f(ival), f(f(ival)), ...]
func Iterate[T any](f func(T) T, ival T, limit int) []T {
	var irec func([]T, int) []T
	irec = func(acc []T, left int) []T {
		if left <= 0 {
			return acc
		}
		return irec(append(acc, f(Last(acc))), left-1)
	}
	return irec([]T{ival}, limit-1)
}

// Reverse returns the reversed version of the list given to it.
func Reverse[T any](coll []T) []T {
	return ReduceRight(Append[T], []T{}, coll)
}

// SplitLines takes a string and returns the string split by delim.
func SplitBy(in string, delim rune) []string {
	slf := func(_, cur rune) bool {
		return cur == delim
	}
	strf := func(in []rune) string {
		if First(in) == delim {
			return string(Drop(1, in))
		}
		return string(in)
	}
	return Map(strf, PartitionBy(slf, []rune(in)))
}
