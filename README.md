# fngo

Functions (mostly list-related) from the functional programming world
ported-ish to go :D

## Usage

Examples uses are in the [library itself](./fngo.go) and the in the
[tests](./fngo_tests.go).

## Functions provided:

```
package fngo // import "codeberg.org/BubbyRoosh/fngo"

Package fngo contains many functions and types to help with a more
functional style of programming.

FUNCTIONS

func Add[T Number](a, b T) T
    Adds a and b.

func Append[T any](coll []T, add T) []T
    Append is a helper functions to be able to use append as a function.

func ButLast[T any](n int, coll []T) []T
    ButLast returns all elements of a list except for the last n amount.

func Contains[T comparable](coll []T, want T) bool
    Contains checks if a list contains an element.

func Dec[T Number](a T) T
    Dec decrements a number by 1.

func Dedupe[T comparable](coll []T) []T
    Dedupe removes successive duplicates of a value.

func Distinct[T comparable](coll []T) []T
    Distinct returns a list with any duplicates removed.

func Div[T Number](a, b T) T
    Divides a and b.

func Drop[T any](n int, coll []T) []T
    Drop returns the list after the first n elements.

func EQ[T comparable](a, b T) bool
    Returns if a is equal to b.

func Empty[T any](coll []T) bool
    Empty returns if a list is empty.

func Equal[T comparable](x, y []T) bool
    Equal checks if the elements of two lists are equal.

func Even(a int) bool
    Returns if a is even.

func Filter[T any](f func(T) bool, coll []T) []T
    Filter removes all elements from a list according to f.

func First[T any](coll []T) T
    First returns the first element of a list.

func Frequencies[T comparable](coll []T) map[T]int
    Frequencies returns a map of all the different elements in a list and how
    many occurrences there are of each element.

func GT[T Number](a, b T) bool
    Returns if a is greater than b.

func GroupBy[K comparable, V any](f func(V) K, coll []V) map[K][]V
    GroupBy takes a monadic function f that returns a key for the returned map.
    f is applied to each element of coll, and element is put in a list at the
    return value of f in the map.

func Identity[T any](id T) T
    Identity returns the argument passed to it.

func Inc[T Number](a T) T
    Inc increments a number by 1.

func Iterate[T any](f func(T) T, ival T, limit int) []T
    Iterate takes a monadic function f and calls f(Last(returnList)) recursively
    until limit is met. [ival, f(ival), f(f(ival)), ...]

func Juxt[I, O any](in I, fs ...func(I) O) []O
    Juxt takes an input value and a list of functions, returning a list of the
    return values from the functions being called on the input value
    individually.

func Keys[K comparable, V any](mp map[K]V) []K
    Keys returns all of the keys within a map.

func LT[T Number](a, b T) bool
    Returns if a is less than b.

func Last[T any](coll []T) T
    Last returns the last element of a list.

func Len[T any](a []T) int
    Returns the length of a.

func Map[I, O any](f func(I) O, mp []I) []O
    Map takes a function f, with the current element, returning an output
    element, and applies f to all elements in the input list, returning a list
    of the output type.

func MapEqual[K, V comparable](x, y map[K]V) bool
    MapEqual checks if the elements of two maps are equal.

func MapMap[K, O comparable, V, A any](
	f func(K, V) (O, A),
	mp map[K]V,
) map[O]A
    MapMap is like Map, but for maps! :D

func Mod(a, b int) int
    Applies modulus to a and b.

func Mul[T Number](a, b T) T
    Multiplies a and b.

func NEQ[T comparable](a, b T) bool
    Returns if a is not equal to b.

func Odd(a int) bool
    Returns if a is odd.

func Partial[I, A, O any](f func(I, A) O, p I) func(A) O
    Partial returns a function with 1 argument, where f has two arguments, and
    the first of the arguments is passed to Partial.

func PartialSecond[I, A, O any](f func(I, A) O, p A) func(I) O
    Partial returns a function with 1 argument, where f has two arguments, and
    the second of the arguments is passed to Partial.

func Partition[T any](n int, coll []T) [][]T
    Partition is like PartitionStep, but the step is the same number as n.

func PartitionBy[T any](f func(T, T) bool, coll []T) [][]T
    PartitionBy takes a function to compare the previous and current value and a
    collection to apply with. If the function returns true, a new sub-list is
    created in the partitioned list.

func PartitionStep[T any](n, step int, coll []T) [][]T
    PartitionStep returns a list of lists of the input list separated (or
    "partitioned") into the smaller equal-length sub-lists, scanned by the
    amount left after stepping.

func Range(start, end, step int) []int
    Range returns a list starting at start and doesn't go past end, stepping in
    intervals of step.

func RangeTo(end int) []int
    RangeTo returns Range(0, n, 1)

func ReduceLeft[I, O any](f func(O, I) O, init O, in []I) O
    ReduceLeft takes a function f, with an accumulator and the current element
    of the list. The accumulator is initialized to the init argument, and is
    returned after the function. Goes left to right.

func ReduceRight[I, O any](f func(O, I) O, init O, in []I) O
    ReduceRight takes a function f, with an accumulator and the current element
    of the list. The accumulator is initialized to the init argument, and is
    returned after the function. Goes right to left.

func Reductions[I, O any](f func(O, I) O, init O, in []I) []O
    Reductions takes a funciton f, with an accumulator and the current element
    of the list. The accumulator is initialized to the init argument, and each
    iteration of the accumulator is returned after the function as a list.
    Another common name for Reductions is Scan.

func Rest[T any](coll []T) []T
    Rest returns every element of a list besides the Head.

func Reverse[T any](coll []T) []T
    Reverse returns the reversed version of the list given to it.

func Rune(a int) rune
    Rune takes an int and converts it to a rune.

func SplitAt[T any](n int, coll []T) [][]T
    SplitAt takes a collection and splits it into two sub-lists, split at the
    index of n.

func SplitBy(in string, delim rune) []string
    SplitLines takes a string and returns the string split by delim.

func StrLen(a string) int
    Returns the length of string a.

func String(a rune) string
    String takes a rune and converts it to a string.

func Sub[T Number](a, b T) T
    Subtracts a and b.

func Take[T any](n int, coll []T) []T
    Take returns the list up to n elements.

func Vals[K comparable, V any](mp map[K]V) []V
    Vals returns all of the values within a map.

func Zero[T Number](a T) bool
    Returns if a is zero.


TYPES

type Number interface {
	constraints.Integer | constraints.Float
}
    Number is a type interface meant to be either integer or float.

type Tuple[O, T any] struct {
	O O
	T T
}
    Tuple is a struct containing 2 values of 2 types (or the same type).

func NewTuple[O, T any](o O, t T) Tuple[O, T]
    NewTuple returns a Tuple[O, T] with the values provided.

func Zip[O, T any](ones []O, twos []T) []Tuple[O, T]
    Zip calls ZipWith, using NewTuple[O, T] as the function.

func ZipWith[O, T any](
	f func(O, T) Tuple[O, T],
	ones []O,
	twos []T,
) []Tuple[O, T]
    ZipWith takes a function f that returns a Tuple[O, T], a list of Os, and a
    list of Ts, and merges the two lists with f.
```
