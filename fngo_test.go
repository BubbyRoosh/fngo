package fngo

import "testing"

func TestEqual(t *testing.T) {
	c1 := []int{1, 2, 3}
	c2 := []int{1, 2, 3}
	c3 := []int{1, 2, 3, 4}
	c4 := []int{2, 3, 4}
	if !Equal(c1, c2) || Equal(c2, c3) || Equal(c2, c4) {
		t.Fatal("Equal failed")
	}
}

func TestHead(t *testing.T) {
	coll := []int{1, 2, 3, 4}
	want := 1
	got := First(coll)
	if want != got {
		t.Fatalf("First failed: wanted %v, got %v", want, got)
	}
}

func TestRest(t *testing.T) {
	coll := []int{1, 2, 3, 4}
	want := []int{2, 3, 4}
	got := Rest(coll)
	if !Equal(want, got) {
		t.Fatalf("Rest failed: wanted %v, got %v", want, got)
	}
}

func TestContains(t *testing.T) {
	coll := []int{1, 2, 3, 4}
	want := 3
	notwant := 5
	if !Contains(coll, want) || Contains(coll, notwant) {
		t.Fatal("Contains failed")
	}
}

func TestReduceRange(t *testing.T) {
	coll := Range(1, 5, 1)
	want := 10
	got := ReduceLeft(Add[int], 0, coll)
	if want != got {
		t.Fatalf("Reduce/Range failed: wanted %v, got %v", want, got)
	}
}

func TestPartialMap(t *testing.T) {
	coll := []int{2, 3, 4}
	want := []int{4, 6, 8}
	got := Map(Partial(Mul[int], 2), coll)
	if !Equal(want, got) {
		t.Fatalf("Map/Partial failed: wanted %v, got %v", want, got)
	}
}

func TestReverse(t *testing.T) {
	coll := []int{1, 2, 3, 4, 5, 6}
	want := []int{6, 5, 4, 3, 2, 1}
	got := Reverse(coll)
	if !Equal(want, got) {
		t.Fatalf("Reverse failed: wanted %v, got %v", want, got)
	}
}

func TestZip(t *testing.T) {
	names := []string{"Bob", "Joe", "Steve"}
	want := []Tuple[int, string]{{0, "Bob"}, {1, "Joe"}, {2, "Steve"}}
	got := Zip(RangeTo(len(names)), names)
	if !Equal(want, got) {
		t.Fatalf("Zip failed: wanted %v, got %v", want, got)
	}
}

func TestReductions(t *testing.T) {
	coll := []int{1, 1, -1, -1, -1, 1}
	want := []int{1, 2, 1, 0, -1, 0}
	got := Reductions(Add[int], 0, coll)
	if !Equal(want, got) {
		t.Fatalf("Reductions failed: wanted %v, got %v", want, got)
	}
}

func TestFrequencies(t *testing.T) {
	str := "abbcccdddd"
	want := map[string]int{
		"a": 1,
		"b": 2,
		"c": 3,
		"d": 4,
	}
	mm := func(r rune, i int) (string, int) {
		return string(r), i
	}
	got := MapMap(mm, Frequencies([]rune(str)))
	if !MapEqual(want, got) {
		t.Fatalf("Frequencies failed: wanted %v, got %v", want, got)
	}
}

func TestDistinct(t *testing.T) {
	coll := []int{1, 2, 3, 1, 4, 3, 2, 1}
	want := []int{1, 2, 3, 4}
	got := Distinct(coll)
	if !Equal(want, got) {
		t.Fatalf("Distinct failed: wanted %v got, %v", want, got)
	}
}

func TestPartition(t *testing.T) {
	coll := RangeTo(8)
	want := [][]int{{0, 1, 2}, {3, 4, 5}}
	got := Partition(3, coll)
	for i, e := range want {
		if len(want) != len(got) || !Equal(got[i], e) {
			t.Fatalf("Partition failed: wanted %v got, %v", want, got)
		}
	}
}

func TestPartitionBy(t *testing.T) {
	coll := []int{1, 2, 3, 2, 3, 4, 3, 4, 5}
	want := [][]int{{1, 2, 3}, {2, 3, 4}, {3, 4, 5}}
	got := PartitionBy(GT[int], coll)
	for i, e := range want {
		if len(want) != len(got) || !Equal(got[i], e) {
			t.Fatalf("PartitionBy failed: wanted %v got, %v", want, got)
		}
	}
}

func TestPartitionStep(t *testing.T) {
	coll := RangeTo(5)
	want := [][]int{{0, 1, 2}, {1, 2, 3}, {2, 3, 4}}
	got := PartitionStep(3, 1, coll)
	for i, e := range want {
		if len(want) != len(got) || !Equal(got[i], e) {
			t.Fatalf("PartitionStep failed: wanted %v got, %v", want, got)
		}
	}
}

func TestDedupe(t *testing.T) {
	coll := []int{1, 1, 1, 1, 1, 2, 3, 3, 3, 2, 2, 1}
	want := []int{1, 2, 3, 2, 1}
	got := Dedupe(coll)
	if !Equal(want, got) {
		t.Fatalf("Dedupe failed: wanted %v got, %v", want, got)
	}
}

func TestJuxt(t *testing.T) {
	coll := []int{1, 2, 3}
	want := [][]int{{2, 4, 6}, {1, 2, 3}, {1, 3}, {2}}
	got := Juxt(
		coll,
		Partial(Map[int, int], Partial(Mul[int], 2)),
		Identity[[]int],
		Partial(Filter[int], Odd),
		Partial(Filter[int], Even))

	for i, e := range want {
		if len(want) != len(got) || !Equal(got[i], e) {
			t.Fatalf("Juxt failed: wanted %v got, %v", want, got)
		}
	}
}

func TestGroupBy(t *testing.T) {
	coll := []string{"a", "as", "asd", "aa", "asdf", "qwer"}
	want := map[int][]string{
		1: {"a"},
		2: {"as", "aa"},
		3: {"asd"},
		4: {"asdf", "qwer"},
	}
	got := GroupBy(StrLen, coll)

	for i, e := range want {
		if len(want) != len(got) || !Equal(got[i], e) {
			t.Fatalf("GroupBy failed: wanted %v got, %v", want, got)
		}
	}
}

func TestIterate(t *testing.T) {
	want := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	got := Iterate(Inc[int], 1, 10)
	if !Equal(want, got) {
		t.Fatalf("Iterate failed: wanted %v got, %v", want, got)
	}
}

func TestSplitBy(t *testing.T) {
	str := "some\nlines\nof input!\n:D"
	want := []string{"some", "lines", "of input!", ":D"}
	got := SplitBy(str, '\n')
	if !Equal(want, got) {
		t.Fatalf("Iterate failed: wanted %v got, %v", want, got)
	}
}
